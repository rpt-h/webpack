const path = require('path');
const HtmlPlugin = require('html-webpack-plugin')

const htmlPlugin = new HtmlPlugin({
    //原页面路径
    template: './src/index.html',
    //复制到的位置
    filename: './index.html'
})

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    //development速度快，production体积小
    mode: 'development',
    devtool: 'nosources-source-map',//错误提示从bundle.js回到原代码
    resolve : {
      alias: {
          '@': path.join(__dirname,'./src/')
      }
    },
    //指定处理的文件，默认入口为src/index.js
    entry: path.join(__dirname, './src/index.js'),
    output: {
        path: path.join(__dirname, './dist'),//输出文件位置
        filename: 'js/bundle.js'//输出文件名称
    },
    //插件的数组
    plugins: [htmlPlugin, new CleanWebpackPlugin()],
    devServer: {
        //打包成功后自动打开浏览器
        open: true,
        //指定端口
        port: 8080,
        host: 'localhost'
    },
    //处理非.js文件
    module: {
        rules: [
            //处理.css
            {test: /\.css$/, use: ['style-loader', 'css-loader']},
            //处理.less
            {test: /\.less$/, use: ['style-loader', 'css-loader', 'less-loader']},
            //处理.png|jpg|gif,limit限制转换成base64的大小阈值，超过不转换
            {test: /\.jpg|png|gif/, use: 'url-loader?limit=2000&outputPath=images'},
            //使用babel处理高级的js语法, 不用管第三包的兼容性问题
            {test: /\.js$/, use: 'babel-loader', exclude: /node-modules/}
        ]
    }
}
